package com.example.simplerecyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.simplerecyclerview.databinding.GridItemMenuBinding
import com.example.simplerecyclerview.databinding.LinearItemMenuBinding

class MenuAdapter(val isGrid: Boolean, val data: List<String>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (isGrid) {
            val context = parent.context
            val inflater = LayoutInflater.from(context)
            // Inflate the custom layout
            val contactView = inflater.inflate(R.layout.grid_item_menu, parent, false)
            // Return a new holder instance
            return GridMenuHolder(contactView)
        } else {
            val binding =
                LinearItemMenuBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return LinearMenuHolder(binding)
        }
    }

    //bertujuan untuk mencari tau banyaknya data yang ditampilkan.
    override fun getItemCount(): Int {
        return data.size
    }

    //bertujuan untuk mengikat view ke recyclerview berdasarkan posisi
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (isGrid) {
            val gridHolder = holder as GridMenuHolder
            gridHolder.onBind(data[position])
        } else {
            val linearHolder = holder as LinearMenuHolder
            linearHolder.onBind(data[position])
        }
    }
}

class GridMenuHolder(val itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun onBind(data: String) {
        val tvTitle = itemView.findViewById<TextView>(R.id.title_menu_grid)
        tvTitle.text = data
    }
}

class LinearMenuHolder(val binding: LinearItemMenuBinding) : RecyclerView.ViewHolder(binding.root) {
    fun onBind(data: String) {
        binding.titleMenu.text = data
    }
}