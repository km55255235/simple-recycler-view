package com.example.simplerecyclerview

data class SimpleMenu(
    val title: String,
    val price: String
)
